package com.amin.energy;

import com.amin.energy.api.request.FractionRequest;
import com.amin.energy.api.request.MeterReadingRequest;
import com.amin.energy.api.request.ProfileRequest;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.MeterReading;
import com.amin.energy.model.Month;
import com.amin.energy.model.Profile;

import java.math.BigDecimal;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
public class TestUtil {

    public static MeterReadingRequest getMeterReadingRequest(int meterId, String month, Long meterReading, String profile) {
        MeterReadingRequest meterReadingRequest = new MeterReadingRequest();
        meterReadingRequest.setMeterId(meterId);
        meterReadingRequest.setMonth(month);
        meterReadingRequest.setMeterReading(meterReading);
        meterReadingRequest.setProfile(profile);

        return meterReadingRequest;
    }

    public static MeterReading getMeterReading(int meterId, String month, Long currentMeterReading) {
        MeterReading meterReading = new MeterReading();
        meterReading.setMeterId(meterId);
        meterReading.setMonth(Month.getValueByName(month));
        meterReading.setMeterReading(currentMeterReading);

        return meterReading;
    }

    public static FractionRequest getFractionRequest(String fraction, String month, String profile) {
        FractionRequest fractionRequest = new FractionRequest();
        fractionRequest.setFraction(new BigDecimal(fraction));
        fractionRequest.setMonth(month);
        fractionRequest.setProfile(profile);

        return fractionRequest;
    }

    public static Fraction getFraction(String ratio, String month) {
        Fraction fraction = new Fraction();
        fraction.setRatio(new BigDecimal(ratio));
        fraction.setMonth(Month.getValueByName(month));

        return fraction;
    }

    public static ProfileRequest getProfileRequest(String title) {
        ProfileRequest profileRequest = new ProfileRequest();
        profileRequest.setTitle(title);

        return profileRequest;
    }

    public static Profile getProfile(String title) {
        Profile profile = new Profile();
        profile.setTitle(title);

        return profile;
    }

}
