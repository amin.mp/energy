package com.amin.energy;

import com.amin.energy.api.FractionController;
import com.amin.energy.api.MeterReadingController;
import com.amin.energy.api.MigrationController;
import com.amin.energy.api.ProfileController;
import com.amin.energy.model.dao.FractionRepository;
import com.amin.energy.model.dao.MeterReadingRepository;
import com.amin.energy.model.dao.ProfileRepository;
import com.amin.energy.service.FractionService;
import com.amin.energy.service.MeterReadingService;
import com.amin.energy.service.MigrationService;
import com.amin.energy.service.ProfileService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EnergyApplicationTests {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(applicationContext.getBean(FractionController.class));
        Assert.assertNotNull(applicationContext.getBean(MeterReadingController.class));
        Assert.assertNotNull(applicationContext.getBean(ProfileController.class));
        Assert.assertNotNull(applicationContext.getBean(MigrationController.class));

        Assert.assertNotNull(applicationContext.getBean(FractionService.class));
        Assert.assertNotNull(applicationContext.getBean(MeterReadingService.class));
        Assert.assertNotNull(applicationContext.getBean(ProfileService.class));
        Assert.assertNotNull(applicationContext.getBean(MigrationService.class));

        Assert.assertNotNull(applicationContext.getBean(FractionRepository.class));
        Assert.assertNotNull(applicationContext.getBean(MeterReadingRepository.class));
        Assert.assertNotNull(applicationContext.getBean(ProfileRepository.class));
    }

}
