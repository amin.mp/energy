package com.amin.energy.api;

import com.amin.energy.api.response.ErrorResponse;
import com.amin.energy.model.dao.FractionRepository;
import com.amin.energy.model.dao.MeterReadingRepository;
import com.amin.energy.model.dao.ProfileRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MigrationControllerIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private FractionRepository fractionRepository;

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Before
    public void setUp() {
        meterReadingRepository.deleteAll();
        fractionRepository.deleteAll();
        profileRepository.deleteAll();
    }

    @Test
    public void importMeterReading_withValidId_shouldDeleteTheAppropriateProfile() throws URISyntaxException {

        File fractionDataFile = new File(getClass().getClassLoader().getResource("fractions.txt").getFile());
        URI fractionMigrationUri = UriComponentsBuilder
                .fromHttpUrl(String.format("http://localhost:%d/migration/fraction", port))
                .queryParam("path", fractionDataFile.getAbsolutePath())
                .build().toUri();

        ResponseEntity<?> actualFractionMigrationResponse = testRestTemplate.exchange(fractionMigrationUri, HttpMethod.POST,
                null, ErrorResponse.class);

        Assert.assertThat(actualFractionMigrationResponse.getStatusCode(), equalTo(HttpStatus.OK));


        File meterReadingDataFile = new File(getClass().getClassLoader().getResource("meter-readings.txt").getFile());
        URI meterReadingMigrationUri = UriComponentsBuilder
                .fromHttpUrl(String.format("http://localhost:%d/migration/meterReading", port))
                .queryParam("path", meterReadingDataFile.getAbsolutePath())
                .build().toUri();

        ResponseEntity<?> actualMeterReadingMigrationResponse = testRestTemplate.exchange(meterReadingMigrationUri, HttpMethod.POST,
                null, ErrorResponse.class);

        Assert.assertThat(actualMeterReadingMigrationResponse.getStatusCode(), equalTo(HttpStatus.OK));
    }

}
