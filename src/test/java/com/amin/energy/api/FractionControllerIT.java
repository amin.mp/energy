package com.amin.energy.api;

import com.amin.energy.TestUtil;
import com.amin.energy.api.request.FractionRequest;
import com.amin.energy.api.response.ErrorResponse;
import com.amin.energy.api.response.FractionResponse;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.Profile;
import com.amin.energy.model.dao.FractionRepository;
import com.amin.energy.model.dao.MeterReadingRepository;
import com.amin.energy.model.dao.ProfileRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FractionControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private FractionRepository fractionRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    private Profile profile;
    private Fraction fraction;

    @Before
    public void setUp() {
        meterReadingRepository.deleteAll();
        fractionRepository.deleteAll();
        profileRepository.deleteAll();

        profile = TestUtil.getProfile("F");
        profile = profileRepository.save(profile);

        fraction = TestUtil.getFraction("0.1", "DEC");
        fraction.setProfile(profile);
        fraction = fractionRepository.save(fraction);

    }

    @Test
    public void insert_withValidRequest_newFractionShouldBeInserted() {
        FractionRequest fractionRequest = TestUtil.getFractionRequest("0.2", "JAN", "F");

        HttpEntity<FractionRequest> fractionRequestHttpEntity = new HttpEntity<>(fractionRequest);
        ResponseEntity<FractionResponse> actualResponse = testRestTemplate.exchange("/fraction", HttpMethod.POST,
                fractionRequestHttpEntity, FractionResponse.class, "");

        FractionResponse actualFractionResponse = actualResponse.getBody();

        Assert.assertThat(actualFractionResponse.getId(), notNullValue());
        Assert.assertThat(actualFractionResponse.getMonth(), is("JAN"));
        Assert.assertThat(actualFractionResponse.getProfile(), is("F"));
    }

    @Test
    public void load_withValidId_returnsAppropriateFraction() {
        ResponseEntity<FractionResponse> actualResponse = testRestTemplate.exchange("/fraction/{id}", HttpMethod.GET,
                null, FractionResponse.class, fraction.getId());

        FractionResponse actualFractionResponse = actualResponse.getBody();

        Assert.assertThat(actualFractionResponse.getId(), equalTo(fraction.getId()));
        Assert.assertThat(actualFractionResponse.getMonth(), is("DEC"));
        Assert.assertThat(actualFractionResponse.getProfile(), is("F"));
    }

    @Test
    public void load_withInvalidId_returnsErrorResponse() {

        final int INVALID_FRACTION_ID = -1;

        ErrorResponse expectedErrorResponse = new ErrorResponse(ErrorCode.NOT_FOUND.getValue().value(),
                "not found");

        ResponseEntity<ErrorResponse> actualResponse = testRestTemplate.exchange("/fraction/{id}",
                HttpMethod.GET, null, ErrorResponse.class, INVALID_FRACTION_ID);

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(expectedErrorResponse.getErrorCode())));
        Assert.assertThat(actualResponse.getBody().getErrorMessage(), containsString(expectedErrorResponse.getErrorMessage()));
        Assert.assertThat(actualResponse.getBody().getTimestamp(), notNullValue());
    }

    @Test
    public void update_withValidRequest_returnsUpdatedFraction() {
        FractionRequest fractionRequest = TestUtil.getFractionRequest("0.2", "AUG", "F");

        HttpEntity<FractionRequest> fractionRequestHttpEntity = new HttpEntity<>(fractionRequest);
        ResponseEntity<FractionResponse> actualResponse = testRestTemplate.exchange("/fraction/{id}", HttpMethod.PUT,
                fractionRequestHttpEntity, FractionResponse.class, fraction.getId());

        FractionResponse actualFractionResponse = actualResponse.getBody();

        Assert.assertThat(actualFractionResponse.getId(), equalTo(fraction.getId()));
        Assert.assertThat(actualFractionResponse.getMonth(), is(fractionRequest.getMonth()));
        Assert.assertThat(actualFractionResponse.getProfile(), is(fractionRequest.getProfile()));
    }

    @Test
    public void delete_withValidId_shouldDeleteTheAppropriateFraction() {
        ResponseEntity<?> actualResponse = testRestTemplate.exchange("/fraction/{id}", HttpMethod.DELETE,
                null, FractionResponse.class, fraction.getId());

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.OK));

        Assert.assertThat(fractionRepository.findById(fraction.getId()).isPresent(), is(false));
    }

}
