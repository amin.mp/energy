package com.amin.energy.api;

import com.amin.energy.TestUtil;
import com.amin.energy.api.request.MeterReadingRequest;
import com.amin.energy.api.response.ConsumptionResponse;
import com.amin.energy.api.response.ErrorResponse;
import com.amin.energy.api.response.MeterReadingResponse;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.MeterReading;
import com.amin.energy.model.Month;
import com.amin.energy.model.Profile;
import com.amin.energy.model.dao.FractionRepository;
import com.amin.energy.model.dao.MeterReadingRepository;
import com.amin.energy.model.dao.ProfileRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import static org.hamcrest.CoreMatchers.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeterReadingControllerIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private FractionRepository fractionRepository;

    @Autowired
    private ProfileRepository profileRepository;

    private MeterReading januaryMeterReading;
    private MeterReading februaryMeterReading;
    private MeterReading marchMeterReading;

    private Profile profile;
    private Fraction januaryFraction;
    private Fraction februaryFraction;
    private Fraction marchFraction;
    private Fraction decemberFraction;

    private final int SAMPLE_METER_ID = 1;

    @Before
    public void setUp() {
        meterReadingRepository.deleteAll();

        fractionRepository.deleteAll();

        profileRepository.deleteAll();

        profile = TestUtil.getProfile("M");
        profile = profileRepository.save(profile);

        januaryFraction = TestUtil.getFraction("0.1", "JAN");
        januaryFraction.setProfile(profile);
        januaryFraction = fractionRepository.save(januaryFraction);

        februaryFraction = TestUtil.getFraction("0.1", "FEB");
        februaryFraction.setProfile(profile);
        februaryFraction = fractionRepository.save(februaryFraction);

        marchFraction = TestUtil.getFraction("0.1", "MAR");
        marchFraction.setProfile(profile);
        marchFraction = fractionRepository.save(marchFraction);

        decemberFraction = TestUtil.getFraction("0.1", "DEC");
        decemberFraction.setProfile(profile);
        decemberFraction = fractionRepository.save(decemberFraction);

        januaryMeterReading = TestUtil.getMeterReading(SAMPLE_METER_ID, "JAN", 10L);
        januaryMeterReading.setProfile(profile);
        januaryMeterReading = meterReadingRepository.save(januaryMeterReading);

        februaryMeterReading = TestUtil.getMeterReading(SAMPLE_METER_ID, "FEB", 30L);
        februaryMeterReading.setProfile(profile);
        februaryMeterReading = meterReadingRepository.save(februaryMeterReading);

        marchMeterReading = TestUtil.getMeterReading(SAMPLE_METER_ID, "MAR", 60L);
        marchMeterReading.setProfile(profile);
        marchMeterReading = meterReadingRepository.save(marchMeterReading);
    }

    @Test
    public void getConsumption_withValidRequest_returnsConsumption() {
        UriComponentsBuilder consumptionUriBuilder = UriComponentsBuilder
                .fromHttpUrl(String.format("http://localhost:%d/meterReading/consumption", port))
                .queryParam("meterId", januaryMeterReading.getMeterId());

        ResponseEntity<ConsumptionResponse> januaryActualResponse = testRestTemplate.exchange(
                consumptionUriBuilder.queryParam("month", Month.getMonth(januaryMeterReading.getMonth()).name()).build().toUri()
                , HttpMethod.GET, null, ConsumptionResponse.class);
        ConsumptionResponse januaryActualConsumption = januaryActualResponse.getBody();

        Assert.assertThat(januaryActualConsumption, notNullValue());
        Assert.assertThat(januaryActualConsumption.getConsumption().longValue(), is(10L));

        ResponseEntity<ConsumptionResponse> februaryActualResponse = testRestTemplate.exchange(
                consumptionUriBuilder.replaceQueryParam("month", Month.getMonth(februaryMeterReading.getMonth()).name()).build().toUri()
                , HttpMethod.GET, null, ConsumptionResponse.class);
        ConsumptionResponse februaryActualConsumption = februaryActualResponse.getBody();

        Assert.assertThat(februaryActualConsumption, notNullValue());
        Assert.assertThat(februaryActualConsumption.getConsumption().longValue(), is(20L));

        ResponseEntity<ConsumptionResponse> marchActualResponse = testRestTemplate.exchange(
                consumptionUriBuilder.replaceQueryParam("month", Month.getMonth(marchMeterReading.getMonth()).name()).build().toUri()
                , HttpMethod.GET, null, ConsumptionResponse.class);
        ConsumptionResponse marchActualConsumption = marchActualResponse.getBody();

        Assert.assertThat(marchActualConsumption, notNullValue());
        Assert.assertThat(marchActualConsumption.getConsumption().longValue(), is(30L));
    }

    @Test
    public void insert_withValidRequest_newMeterReadingShouldBeInserted() {
        MeterReadingRequest meterReadingRequest = TestUtil.getMeterReadingRequest(0001, "DEC", 1500L, "M");

        HttpEntity<MeterReadingRequest> meterReadingRequestHttpEntity = new HttpEntity<>(meterReadingRequest);
        ResponseEntity<MeterReadingResponse> actualResponse = testRestTemplate.exchange("/meterReading", HttpMethod.POST,
                meterReadingRequestHttpEntity, MeterReadingResponse.class, "");

        MeterReadingResponse actualMeterReadingResponse = actualResponse.getBody();

        Assert.assertThat(actualMeterReadingResponse.getId(), notNullValue());
        Assert.assertThat(actualMeterReadingResponse.getMeterReading(), is(meterReadingRequest.getMeterReading()));
        Assert.assertThat(actualMeterReadingResponse.getMeterId(), is(meterReadingRequest.getMeterId()));
        Assert.assertThat(actualMeterReadingResponse.getMonth(), is(meterReadingRequest.getMonth()));
        Assert.assertThat(actualMeterReadingResponse.getProfile(), is(meterReadingRequest.getProfile()));
    }

    @Test
    public void load_withValidId_returnsAppropriateMeterReading() {
        ResponseEntity<MeterReadingResponse> actualResponse = testRestTemplate.exchange("/meterReading/{id}", HttpMethod.GET,
                null, MeterReadingResponse.class, januaryMeterReading.getId());

        MeterReadingResponse actualMeterReadingResponse = actualResponse.getBody();

        Assert.assertThat(actualMeterReadingResponse.getId(), equalTo(januaryMeterReading.getId()));
        Assert.assertThat(actualMeterReadingResponse.getMeterReading(), is(januaryMeterReading.getMeterReading()));
        Assert.assertThat(actualMeterReadingResponse.getMeterId(), is(januaryMeterReading.getMeterId()));
        Assert.assertThat(actualMeterReadingResponse.getMonth(), is(Month.getMonth(januaryMeterReading.getMonth()).name()));
        Assert.assertThat(actualMeterReadingResponse.getProfile(), is(januaryMeterReading.getProfile().getTitle()));
    }

    @Test
    public void load_withInvalidId_returnsErrorResponse() {

        final int INVALID_METER_READING_ID = -1;

        ErrorResponse expectedErrorResponse = new ErrorResponse(ErrorCode.NOT_FOUND.getValue().value(),
                "not found");

        ResponseEntity<ErrorResponse> actualResponse = testRestTemplate.exchange("/meterReading/{id}",
                HttpMethod.GET, null, ErrorResponse.class, INVALID_METER_READING_ID);

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(expectedErrorResponse.getErrorCode())));
        Assert.assertThat(actualResponse.getBody().getErrorMessage(), containsString(expectedErrorResponse.getErrorMessage()));
        Assert.assertThat(actualResponse.getBody().getTimestamp(), notNullValue());
    }

    @Test
    public void update_withValidRequest_returnsUpdatedFraction() {
        MeterReadingRequest meterReadingRequest = TestUtil.getMeterReadingRequest(2, "DEC", 1200L, "M");

        HttpEntity<MeterReadingRequest> meterReadingRequestHttpEntity = new HttpEntity<>(meterReadingRequest);
        ResponseEntity<MeterReadingResponse> actualResponse = testRestTemplate.exchange("/meterReading/{id}", HttpMethod.PUT,
                meterReadingRequestHttpEntity, MeterReadingResponse.class, januaryMeterReading.getId());

        MeterReadingResponse actualMeterReadingResponse = actualResponse.getBody();

        Assert.assertThat(actualMeterReadingResponse.getId(), equalTo(januaryMeterReading.getId()));
        Assert.assertThat(actualMeterReadingResponse.getMeterReading(), is(meterReadingRequest.getMeterReading()));
        Assert.assertThat(actualMeterReadingResponse.getMeterId(), is(meterReadingRequest.getMeterId()));
        Assert.assertThat(actualMeterReadingResponse.getMonth(), is(meterReadingRequest.getMonth()));
        Assert.assertThat(actualMeterReadingResponse.getProfile(), is(meterReadingRequest.getProfile()));
    }

    @Test
    public void delete_withValidId_shouldDeleteTheAppropriateMeterReading() {
        ResponseEntity<?> actualResponse = testRestTemplate.exchange("/meterReading/{id}", HttpMethod.DELETE,
                null, MeterReadingResponse.class, januaryMeterReading.getId());

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.OK));

        Assert.assertThat(meterReadingRepository.findById(januaryMeterReading.getId()).isPresent(), is(false));
    }

}
