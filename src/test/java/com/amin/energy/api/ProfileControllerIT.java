package com.amin.energy.api;

import com.amin.energy.TestUtil;
import com.amin.energy.api.request.ProfileRequest;
import com.amin.energy.api.response.ErrorResponse;
import com.amin.energy.api.response.ProfileResponse;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Profile;
import com.amin.energy.model.dao.ProfileRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProfileControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProfileRepository profileRepository;

    @Test
    public void insert_withValidRequest_newProfileShouldBeInserted() {
        ProfileRequest profileRequest = TestUtil.getProfileRequest("X");

        HttpEntity<ProfileRequest> profileRequestHttpEntity = new HttpEntity<>(profileRequest);
        ResponseEntity<ProfileResponse> actualResponse = testRestTemplate.exchange("/profile", HttpMethod.POST,
                profileRequestHttpEntity, ProfileResponse.class, "");

        ProfileResponse actualProfileResponse = actualResponse.getBody();

        Assert.assertThat(actualProfileResponse.getId(), notNullValue());
        Assert.assertThat(actualProfileResponse.getTitle(), is(profileRequest.getTitle()));
    }

    @Test
    public void load_withValidId_returnsAppropriateProfile() {

        Profile expectedProfile = createProfile("A");

        ResponseEntity<ProfileResponse> actualResponse = testRestTemplate.exchange("/profile/{id}", HttpMethod.GET,
                null, ProfileResponse.class, expectedProfile.getId());

        ProfileResponse actualProfileResponse = actualResponse.getBody();

        Assert.assertThat(actualProfileResponse.getId(), equalTo(expectedProfile.getId()));
        Assert.assertThat(actualProfileResponse.getTitle(), is(expectedProfile.getTitle()));
    }

    private Profile createProfile(String title) {
        Profile profile = TestUtil.getProfile(title);
        profile = profileRepository.save(profile);
        return profile;
    }

    @Test
    public void load_withInvalidId_returnsErrorResponse() {

        final int INVALID_PROFILE_ID = -1;

        ErrorResponse expectedErrorResponse = new ErrorResponse(ErrorCode.NOT_FOUND.getValue().value(),
                "not found");

        ResponseEntity<ErrorResponse> actualResponse = testRestTemplate.exchange("/profile/{id}",
                HttpMethod.GET, null, ErrorResponse.class, INVALID_PROFILE_ID);

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(expectedErrorResponse.getErrorCode())));
        Assert.assertThat(actualResponse.getBody().getErrorMessage(), containsString(expectedErrorResponse.getErrorMessage()));
        Assert.assertThat(actualResponse.getBody().getTimestamp(), notNullValue());
    }

    @Test
    public void update_withValidRequest_returnsUpdatedProfile() {

        Profile sampleProfile = createProfile("Y");
        ProfileRequest profileRequest = TestUtil.getProfileRequest("Z");

        HttpEntity<ProfileRequest> profileRequestHttpEntity = new HttpEntity<>(profileRequest);
        ResponseEntity<ProfileResponse> actualResponse = testRestTemplate.exchange("/profile/{id}", HttpMethod.PUT,
                profileRequestHttpEntity, ProfileResponse.class, sampleProfile.getId());

        ProfileResponse actualProfileResponse = actualResponse.getBody();

        Assert.assertThat(actualProfileResponse.getId(), equalTo(sampleProfile.getId()));
        Assert.assertThat(actualProfileResponse.getTitle(), is(profileRequest.getTitle()));
    }

    @Test
    public void delete_withValidId_shouldDeleteTheAppropriateProfile() {
        Profile sampleProfile = createProfile("W");

        ResponseEntity<?> actualResponse = testRestTemplate.exchange("/profile/{id}", HttpMethod.DELETE,
                null, ProfileResponse.class, sampleProfile.getId());

        Assert.assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.OK));

        Assert.assertThat(profileRepository.findById(sampleProfile.getId()).isPresent(), is(false));
    }

}
