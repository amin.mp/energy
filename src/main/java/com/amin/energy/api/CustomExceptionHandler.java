package com.amin.energy.api;

import com.amin.energy.api.response.ErrorResponse;
import com.amin.energy.exception.ApiServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 * It is a custom exception handler which will apply to all Controllers of the application.
 */
@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handles all errors which are in the form of a Exception.class
     *
     * @param exception the cause
     * @return an instance of ErrorResponse with an appropriate status code
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleAllExceptions(Exception exception) {
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(new ErrorResponse(httpStatus.value(), exception.getMessage()), httpStatus);
    }

    /**
     * Handles all errors which are in the form of an ApiServiceException.class
     *
     * @param exception the cause
     * @return an instance of ErrorResponse with an appropriate status code
     */
    @ExceptionHandler(ApiServiceException.class)
    public ResponseEntity<ErrorResponse> handleApiServiceException(ApiServiceException exception) {

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if (exception.getErrorCode().isPresent()) {
            httpStatus = exception.getErrorCode().get().getValue();
        }

        return new ResponseEntity<>(new ErrorResponse(httpStatus.value(), exception.getMessage()), httpStatus);
    }
}
