package com.amin.energy.api;

import com.amin.energy.api.request.MeterReadingRequest;
import com.amin.energy.api.response.ConsumptionResponse;
import com.amin.energy.api.response.MeterReadingResponse;
import com.amin.energy.mapper.MeterReadingMapper;
import com.amin.energy.model.MeterReading;
import com.amin.energy.model.Month;
import com.amin.energy.service.MeterReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@RestController
@RequestMapping("/meterReading")
@Api(description = "Provides set of REST API to calculate consumptions and handle CRUD operations for Meter readings.")
public class MeterReadingController {

    private MeterReadingService meterReadingService;
    private MeterReadingMapper mapper;

    public MeterReadingController(MeterReadingService meterReadingService, MeterReadingMapper mapper) {
        this.meterReadingService = meterReadingService;
        this.mapper = mapper;
    }

    @PostMapping
    @ApiOperation("Adds a new Meter reading to the application.")
    public MeterReadingResponse insert(@RequestBody MeterReadingRequest meterReadingRequest) {
        MeterReading meterReading = mapper.meterReadingRequestToMeterReading(meterReadingRequest);
        return mapper.meterReadingToMeterReadingResponse(meterReadingService.insert(meterReading));
    }

    @GetMapping("/{id}")
    @ApiOperation("Retrieves a Meter reading by its id.")
    public MeterReadingResponse load(@PathVariable("id") Long meterReadingId) {
        return mapper.meterReadingToMeterReadingResponse(meterReadingService.loadById(meterReadingId));
    }

    @PutMapping("/{id}")
    @ApiOperation("Updates the Meter reading with the given id with new data.")
    public MeterReadingResponse update(@PathVariable("id") Long meterReadingId,
                                       @RequestBody MeterReadingRequest meterReadingRequest) {
        MeterReading meterReading = mapper.meterReadingRequestToMeterReading(meterReadingRequest);

        return mapper.meterReadingToMeterReadingResponse(meterReadingService.update(meterReadingId, meterReading));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Deletes the Meter reading with the given id.")
    public ResponseEntity<?> delete(@PathVariable("id") Long meterReadingId) {
        meterReadingService.delete(meterReadingId);

        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/consumption", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Calculate Consumption for a given Meter for a given month.")
    public ConsumptionResponse getConsumption(@RequestParam("meterId") int meterId, @RequestParam("month") String month) {

        Long consumption = meterReadingService.getConsumption(meterId, Month.valueOf(month.toUpperCase()));
        return new ConsumptionResponse(meterId, month, consumption);
    }

}
