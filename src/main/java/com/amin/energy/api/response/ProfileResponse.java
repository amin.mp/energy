package com.amin.energy.api.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@ApiModel(description = "This class represents a Profile Response.")
public class ProfileResponse implements Serializable {

    @ApiModelProperty(notes = "Unique identifier of a profile.", example = "1")
    private Integer id;

    @ApiModelProperty(notes = "Title of a profile.", example = "A")
    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
