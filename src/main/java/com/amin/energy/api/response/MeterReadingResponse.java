package com.amin.energy.api.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@ApiModel(description = "This class represents a meter reading response data.")
public class MeterReadingResponse implements Serializable {

    @ApiModelProperty(notes = "Unique identifier of a meter reading.", example = "1")
    private Long id;

    @ApiModelProperty(notes = "Id of the meter.", example = "1")
    private Integer meterId;

    @ApiModelProperty(notes = "Title of a profile.", example = "A")
    private String profile;

    @ApiModelProperty(notes = "Month name in short format.", example = "JAN, FEB, MAR, ...")
    private String month;

    @ApiModelProperty(notes = "the number that the Meter is showing at a specific date.", example = "100")
    private Long meterReading;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMeterId() {
        return meterId;
    }

    public void setMeterId(Integer meterId) {
        this.meterId = meterId;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Long getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(Long meterReading) {
        this.meterReading = meterReading;
    }
}
