package com.amin.energy.api.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@ApiModel(description = "This class represents consumption data.")
public class ConsumptionResponse implements Serializable {

    @ApiModelProperty(notes = "Id of the meter.", example = "1")
    private Integer meterId;

    @ApiModelProperty(notes = "Month name in short format.", example = "JAN, FEB, MAR, ...")
    private String month;

    @ApiModelProperty(notes = "Represents consumption for a given Meter for a given month.", example = "100")
    private Long consumption;

    public ConsumptionResponse() {
    }

    public ConsumptionResponse(Integer meterId, String month, Long consumption) {
        this.meterId = meterId;
        this.month = month;
        this.consumption = consumption;
    }

    public Integer getMeterId() {
        return meterId;
    }

    public void setMeterId(Integer meterId) {
        this.meterId = meterId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Long getConsumption() {
        return consumption;
    }

    public void setConsumption(Long consumption) {
        this.consumption = consumption;
    }
}
