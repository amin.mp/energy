package com.amin.energy.api.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@ApiModel(description = "This class represents a fraction response.")
public class FractionResponse implements Serializable {

    @ApiModelProperty(notes = "Unique identifier of a fraction.", example = "1")
    private Integer id;

    @ApiModelProperty(notes = "Month name in short format.", example = "JAN, FEB, MAR, ...")
    private String month;

    @ApiModelProperty(notes = "Title of a profile.", example = "A")
    private String profile;

    @ApiModelProperty(notes = "Consumption ratio of a month. 0 <= ration <= 1", example = "0.05")
    private BigDecimal fraction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public BigDecimal getFraction() {
        return fraction;
    }

    public void setFraction(BigDecimal fraction) {
        this.fraction = fraction;
    }
}
