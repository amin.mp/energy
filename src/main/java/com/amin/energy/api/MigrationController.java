package com.amin.energy.api;

import com.amin.energy.service.MigrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@RestController
@RequestMapping("/migration")
@Api(description = "Provides set of endpoints to import fraction & meter reading from the legacy data format.")
public class MigrationController {

    private MigrationService migrationService;

    public MigrationController(MigrationService migrationService) {
        this.migrationService = migrationService;
    }

    @PostMapping("/meterReading")
    @ApiOperation("Imports metre readings from the legacy data format.")
    public ResponseEntity<?> importMeterReading(
            @ApiParam("AbsolutePath to the old-format meter reading CSV file.")
            @RequestParam("path") String meterReadingFilePath) {

        migrationService.importMeterReadings(meterReadingFilePath);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/fraction")
    @ApiOperation("Imports fraction from the legacy data format.")
    public ResponseEntity<?> importFraction(
            @ApiParam("AbsolutePath to the old-format fraction CSV file.")
            @RequestParam("path") String fractionFilePath) {
        migrationService.importFractions(fractionFilePath);
        return ResponseEntity.ok().build();
    }

}
