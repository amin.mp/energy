package com.amin.energy.api;

import com.amin.energy.api.request.ProfileRequest;
import com.amin.energy.api.response.ProfileResponse;
import com.amin.energy.mapper.ProfileMapper;
import com.amin.energy.model.Profile;
import com.amin.energy.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@RestController
@RequestMapping("/profile")
@Api(description = "Provides set of REST API to handle CRUD operations for Profile.")
public class ProfileController {

    private ProfileService profileService;
    private ProfileMapper mapper;

    public ProfileController(ProfileService profileService, ProfileMapper mapper) {
        this.profileService = profileService;
        this.mapper = mapper;
    }

    @PostMapping
    @ApiOperation("Adds a new Profile to the application.")
    public ProfileResponse insert(@RequestBody ProfileRequest profileRequest) {
        Profile profile = mapper.profileRequestToProfile(profileRequest);
        return mapper.profileToProfileResponse(profileService.insert(profile));
    }

    @GetMapping("/{id}")
    @ApiOperation("Retrieves a Profile by its id.")
    public ProfileResponse load(@PathVariable("id") Integer profileId) {
        return mapper.profileToProfileResponse(profileService.loadById(profileId));
    }

    @PutMapping("/{id}")
    @ApiOperation("Updates the Profile with the given id with new data.")
    public ProfileResponse update(@PathVariable("id") Integer profileId,
                                  @RequestBody ProfileRequest profileRequest) {

        Profile profile = mapper.profileRequestToProfile(profileRequest);
        return mapper.profileToProfileResponse(profileService.update(profileId, profile));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Deletes the Profile with the given id.")
    public ResponseEntity<?> delete(@PathVariable("id") Integer profileId) {
        profileService.delete(profileId);

        return ResponseEntity.ok().build();
    }

}
