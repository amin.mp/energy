package com.amin.energy.api;

import com.amin.energy.api.request.FractionRequest;
import com.amin.energy.api.response.FractionResponse;
import com.amin.energy.mapper.FractionMapper;
import com.amin.energy.model.Fraction;
import com.amin.energy.service.FractionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@RestController
@RequestMapping("/fraction")
@Api(description = "Provides set of REST API to handle CRUD operations for Fractions.")
public class FractionController {

    private FractionService fractionService;
    private FractionMapper mapper;

    public FractionController(FractionService fractionService, FractionMapper mapper) {
        this.fractionService = fractionService;
        this.mapper = mapper;
    }

    @PostMapping
    @ApiOperation("Adds a new Fraction to the application.")
    public FractionResponse insert(
            @ApiParam("AbsolutePath to the old-format meter reading CSV file.")
            @RequestBody FractionRequest fractionRequest) {
        Fraction fraction = mapper.fractionRequestToFraction(fractionRequest);
        return mapper.fractionToFractionResponse(fractionService.insert(fraction));
    }

    @GetMapping("/{id}")
    @ApiOperation("Retrieves a Fraction by its id.")
    public FractionResponse load(@PathVariable("id") Integer fractionId) {
        return mapper.fractionToFractionResponse(fractionService.loadById(fractionId));
    }

    @PutMapping("/{id}")
    @ApiOperation("Updates the Fraction with the given id with new data.")
    public FractionResponse update(@PathVariable("id") Integer fractionId,
                                   @RequestBody FractionRequest fractionRequest) {

        Fraction fraction = mapper.fractionRequestToFraction(fractionRequest);
        return mapper.fractionToFractionResponse(fractionService.update(fractionId, fraction));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Deletes the Fraction with the given id.")
    public ResponseEntity<?> delete(@PathVariable("id") Integer fractionId) {
        fractionService.delete(fractionId);

        return ResponseEntity.ok().build();
    }
}
