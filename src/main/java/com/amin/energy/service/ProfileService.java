package com.amin.energy.service;

import com.amin.energy.model.Profile;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
public interface ProfileService {

    void delete(Integer profileId);

    Profile update(Integer profileId, Profile profile);

    Profile loadById(Integer profileId);

    Optional<Profile> findByTitle(String title);

    Profile insert(Profile profile);
}
