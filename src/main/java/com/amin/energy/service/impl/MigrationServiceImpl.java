package com.amin.energy.service.impl;

import com.amin.energy.exception.ApiServiceException;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.mapper.FractionMapper;
import com.amin.energy.mapper.MeterReadingMapper;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.Month;
import com.amin.energy.model.Profile;
import com.amin.energy.model.FractionDto;
import com.amin.energy.model.MeterReadingDto;
import com.amin.energy.service.FractionService;
import com.amin.energy.service.MeterReadingService;
import com.amin.energy.service.MigrationService;
import com.amin.energy.service.ProfileService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Service
public class MigrationServiceImpl implements MigrationService {

    private ProfileService profileService;
    private FractionService fractionService;
    private MeterReadingService meterReadingService;
    private FractionMapper fractionMapper;
    private MeterReadingMapper meterReadingMapper;

    public MigrationServiceImpl(ProfileService profileService, MeterReadingService meterReadingService,
                                FractionService fractionService, FractionMapper fractionMapper,
                                MeterReadingMapper meterReadingMapper) {

        this.profileService = profileService;
        this.fractionService = fractionService;
        this.meterReadingService = meterReadingService;
        this.fractionMapper = fractionMapper;
        this.meterReadingMapper = meterReadingMapper;
    }

    @Override
    public void importMeterReadings(String meterReadingFilePath) {

        List<String> errorList = new LinkedList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(meterReadingFilePath))) {

            Map<Integer, List<MeterReadingDto>> meterReadingMap = reader.lines()
                    .filter(dataLine -> dataLine != null && !dataLine.trim().isEmpty() && !dataLine.toUpperCase().startsWith("METERID"))
                    .map(dataLine -> extractMeterReadingDto(dataLine))
                    .collect(Collectors.groupingBy(MeterReadingDto::getMeterId));

            meterReadingMap.forEach((meterId, meterReadingDtoList) -> {
                meterReadingDtoList.sort((firstDto, secondDto) -> {
                    Integer firstMonth = Month.getValueByName(firstDto.getMonth());
                    Integer secondMonth = Month.getValueByName(secondDto.getMonth());
                    return firstMonth.compareTo(secondMonth);
                });

                Optional<String> errorMessage = validateMeterReading(meterId, meterReadingDtoList);

                if (errorMessage.isPresent()) {
                    errorList.add(errorMessage.get());
                } else {
                    saveMeterReadings(meterReadingDtoList);
                }

            });

        } catch (FileNotFoundException e) {
            throw new ApiServiceException(ErrorCode.INVALID_PARAMETER, "Invalid file path.", e);
        } catch (IOException e) {
            throw new ApiServiceException(ErrorCode.INTERNAL_ERROR, "Internal error.", e);
        }

        try {
            finalizeMigration(meterReadingFilePath, errorList);
        } catch (IOException e) {
            throw new ApiServiceException(ErrorCode.INTERNAL_ERROR, "An error accorded during finalization.", e);
        }
    }

    private void saveMeterReadings(List<MeterReadingDto> meterReadingDtoList) {
        meterReadingDtoList.stream()
                .map(meterReadingDto -> meterReadingMapper.meterReadingDtoToMeterReading(meterReadingDto))
                .forEach(meterReading -> meterReadingService.insert(meterReading));
    }

    private Optional<String> validateMeterReading(Integer meterId, List<MeterReadingDto> meterReadingDtoList) {
        boolean invalidMeterReading = IntStream.range(1, meterReadingDtoList.size())
                .anyMatch(index -> meterReadingDtoList.get(index).getMeterReading() < meterReadingDtoList.get(index - 1).getMeterReading());

        if (invalidMeterReading) {
            return Optional.of(String.format("Error in Meter: %s. Reading for a month should not be lower than the previous one.", meterId));
        }

        List<String> profileList = meterReadingDtoList.stream().map(MeterReadingDto::getProfile).distinct().collect(Collectors.toList());
        if (profileList.size() > 1) {
            return Optional.of(String.format("Error in Meter: %s. More than one profile is associated to the meter readings.", meterId));
        }

        Optional<Profile> profile = profileService.findByTitle(profileList.get(0));
        if (!profile.isPresent()) {
            return Optional.of(String.format("Error in Meter: %s. Given profile %d does not exist.", meterId, profileList.get(0)));
        }

        Fraction exampleFraction = new Fraction();
        exampleFraction.setProfile(profile.get());
        List<Fraction> fractionList = fractionService.findAllByExample(exampleFraction);
        if (fractionList == null || fractionList.size() != 12) {
            return Optional.of(String.format("Error in Meter: %s. Fractions for the given profile %d is is defective.", meterId, profileList.get(0)));
        }
        fractionList.sort(Comparator.comparing(Fraction::getMonth));

        Long totalConsumption = meterReadingDtoList.get(meterReadingDtoList.size() - 1).getMeterReading();
        boolean invalidConsumption = IntStream.range(0, meterReadingDtoList.size())
                .anyMatch(index -> isInconsistentConsumption(getConsumption(meterReadingDtoList, index), totalConsumption, fractionList.get(index)));

        if (invalidConsumption) {
            return Optional.of(String.format("Error in Meter: %s. Consumption is not consistent with the fraction with a tolerance of a 25%.", meterId));
        }

        return Optional.empty();
    }

    private boolean isInconsistentConsumption(Long consumption, Long totalConsumption, Fraction fraction) {

        BigDecimal currentMonthConsumption = BigDecimal.valueOf(consumption);

        BigDecimal ratio = fraction.getRatio();
        BigDecimal totalConsumptionOfYear = BigDecimal.valueOf(totalConsumption);

        BigDecimal averageMonthConsumption = ratio.multiply(totalConsumptionOfYear);

        BigDecimal tolerance = averageMonthConsumption.multiply(new BigDecimal("0.25"));

        BigDecimal upperBound = averageMonthConsumption.add(tolerance);
        BigDecimal lowerBound = averageMonthConsumption.subtract(tolerance);

        boolean isInconsistent = currentMonthConsumption.compareTo(lowerBound) < 0
                || currentMonthConsumption.compareTo(upperBound) > 0;

        return isInconsistent;
    }

    private Long getConsumption(List<MeterReadingDto> meterReadingDtoList, int index) {

        long currentMeterReading = meterReadingDtoList.get(index).getMeterReading();

        long previousMeterReading = 0;
        if (index > 0) {
            previousMeterReading = meterReadingDtoList.get(index - 1).getMeterReading();
        }

        long consumption = currentMeterReading - previousMeterReading;
        return consumption;
    }

    @Override
    public void importFractions(String fractionFilePath) {
        List<String> errorList = new LinkedList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fractionFilePath))) {

            Map<String, List<FractionDto>> profileFractionMap = reader.lines()
                    .filter(dataLine -> dataLine != null && !dataLine.trim().isEmpty() && !dataLine.toUpperCase().startsWith("MONTH"))
                    .map(dataLine -> extractFractionDto(dataLine))
                    .collect(Collectors.groupingBy(FractionDto::getProfile));


            profileFractionMap.forEach((profile, fractionDtoList) -> {
                BigDecimal sumOfFractions = getSumOfFractions(fractionDtoList);
                if (sumOfFractions.compareTo(BigDecimal.ONE) == 0) {
                    saveFractionAndProfile(profile, fractionDtoList);
                } else {
                    errorList.add(String.format("Error in profile: %s. Sum of all fractions is: %s", profile, sumOfFractions.toString()));
                }
            });

        } catch (FileNotFoundException e) {
            throw new ApiServiceException(ErrorCode.INVALID_PARAMETER, "Invalid file path.", e);
        } catch (IOException e) {
            throw new ApiServiceException(ErrorCode.INTERNAL_ERROR, "Internal error.", e);
        }

        try {
            finalizeMigration(fractionFilePath, errorList);
        } catch (IOException e) {
            throw new ApiServiceException(ErrorCode.INTERNAL_ERROR, "An error accorded during finalization.", e);
        }

    }

    private void saveFractionAndProfile(String profileTitle, List<FractionDto> fractionDtoList) {
        Profile profile = profileService.findByTitle(profileTitle)
                .orElseGet(() -> profileService.insert(new Profile(profileTitle)));

        fractionDtoList.stream()
                .map(fractionDto -> fractionMapper.fractionDtoToFraction(fractionDto))
                .forEach(fraction -> {
                    fraction.setProfile(profile);
                    fractionService.insert(fraction);
                });
    }

    private BigDecimal getSumOfFractions(List<FractionDto> fractionDtoList) {
        return fractionDtoList.stream()
                .map(FractionDto::getFraction)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private FractionDto extractFractionDto(String dataLine) {
        String[] data = dataLine.split(",");

        return new FractionDto(data[0].trim(), data[1].trim(), new BigDecimal(data[2].trim()));
    }

    private MeterReadingDto extractMeterReadingDto(String dataLine) {
        String[] data = dataLine.split(",");

        return new MeterReadingDto(new Integer(data[0].trim()), data[1].trim(), data[2].trim(), new Long(data[3].trim()));
    }

    private void finalizeMigration(String inputFilePath, List<String> errorList) throws IOException {
        if (errorList.size() > 0) {
            Files.write(Paths.get(inputFilePath + ".err"), errorList);
        } else {
            Files.delete(Paths.get(inputFilePath));
        }
    }

}
