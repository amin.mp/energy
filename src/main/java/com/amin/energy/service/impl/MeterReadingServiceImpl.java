package com.amin.energy.service.impl;

import com.amin.energy.exception.ApiServiceException;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.MeterReading;
import com.amin.energy.model.Month;
import com.amin.energy.model.dao.MeterReadingRepository;
import com.amin.energy.service.FractionService;
import com.amin.energy.service.MeterReadingService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Service
public class MeterReadingServiceImpl implements MeterReadingService {

    private FractionService fractionService;
    private MeterReadingRepository meterReadingRepository;

    public MeterReadingServiceImpl(FractionService fractionService, MeterReadingRepository meterReadingRepository) {
        this.fractionService = fractionService;
        this.meterReadingRepository = meterReadingRepository;
    }

    @Override
    public long getConsumption(int meterId, Month month) {
        int currentMonth = month.getValue();
        long currentMeterReading;

        currentMeterReading = meterReadingRepository.findMeterReadingByMeterIdAndMonth(meterId, currentMonth)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "No data found for this month."));


        long previousMeterReading = 0;

        if (!month.isFirstMonth()) {
            int previousMonth = currentMonth - 1;
            previousMeterReading = meterReadingRepository.findMeterReadingByMeterIdAndMonth(meterId, previousMonth)
                    .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "No data found for last month."));
        }

        long consumption = currentMeterReading - previousMeterReading;

        return consumption;
    }

    @Override
    public MeterReading insert(MeterReading meterReading) {
        validateMeterReading(meterReading);
        return meterReadingRepository.save(meterReading);
    }

    @Override
    public MeterReading loadById(Long meterReadingId) {
        return meterReadingRepository.findById(meterReadingId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Meter Reading not found"));
    }

    @Override
    public MeterReading update(Long meterReadingId, MeterReading updatedMeterReading) {
        MeterReading meterReading = meterReadingRepository.findById(meterReadingId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Meter Reading not found"));

        validateMeterReading(meterReading);

        meterReading.setMeterId(updatedMeterReading.getMeterId());
        meterReading.setMeterReading(updatedMeterReading.getMeterReading());
        meterReading.setMonth(updatedMeterReading.getMonth());
        meterReading.setProfile(updatedMeterReading.getProfile());

        return meterReadingRepository.save(meterReading);
    }

    @Override
    public void delete(Long meterReadingId) {
        MeterReading meterReading = meterReadingRepository.findById(meterReadingId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Meter Reading not found"));

        meterReadingRepository.delete(meterReading);
    }

    private void validateMeterReading(MeterReading meterReading) {
        Fraction fraction = new Fraction();
        fraction.setProfile(meterReading.getProfile());
        fraction.setMonth(meterReading.getMonth());

        Optional<Fraction> relatedFraction = fractionService.findByExample(fraction);
        if (!relatedFraction.isPresent()) {
            throw new ApiServiceException(ErrorCode.INVALID_PARAMETER, "Related fraction of the meter reading not found");
        }
    }

}
