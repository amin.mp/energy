package com.amin.energy.service.impl;

import com.amin.energy.exception.ApiServiceException;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.dao.FractionRepository;
import com.amin.energy.service.FractionService;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Service
public class FractionServiceImpl implements FractionService {

    private FractionRepository fractionRepository;

    public FractionServiceImpl(FractionRepository fractionRepository) {
        this.fractionRepository = fractionRepository;
    }

    @Override
    public Fraction insert(Fraction fraction) {
        return fractionRepository.save(fraction);
    }

    @Override
    public Optional<Fraction> findByExample(Fraction example) {
        return fractionRepository.findOne(Example.of(example));
    }

    @Override
    public List<Fraction> findAllByExample(Fraction example) {
        return fractionRepository.findAll(Example.of(example));
    }

    @Override
    public Long countByExample(Fraction example) {
        return fractionRepository.count(Example.of(example));
    }

    @Override
    public Fraction loadById(Integer fractionId) {
        return fractionRepository.findById(fractionId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Fraction not found"));
    }

    @Override
    public Fraction update(Integer fractionId, Fraction updatedFraction) {
        Fraction fraction = fractionRepository.findById(fractionId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Fraction not found"));

        fraction.setMonth(updatedFraction.getMonth());
        fraction.setRatio(updatedFraction.getRatio());

        return fractionRepository.save(fraction);
    }

    @Override
    public void delete(Integer fractionId) {
        Fraction fraction = fractionRepository.findById(fractionId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Fraction not found"));

        fractionRepository.delete(fraction);
    }
}
