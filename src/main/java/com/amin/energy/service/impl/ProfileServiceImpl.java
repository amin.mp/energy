package com.amin.energy.service.impl;

import com.amin.energy.exception.ApiServiceException;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Profile;
import com.amin.energy.model.dao.ProfileRepository;
import com.amin.energy.service.ProfileService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Service
public class ProfileServiceImpl implements ProfileService {

    private ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    public Profile insert(Profile profile) {
        return profileRepository.save(profile);
    }

    @Override
    public Profile loadById(Integer profileId) {
        return profileRepository.findById(profileId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Profile not found"));
    }

    @Override
    public Optional<Profile> findByTitle(String title) {
        return profileRepository.findByTitle(title);
    }

    @Override
    public Profile update(Integer profileId, Profile updatedProfile) {
        Profile profile = profileRepository.findById(profileId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Profile not found"));

        profile.setTitle(updatedProfile.getTitle());

        return profileRepository.save(profile);
    }

    @Override
    public void delete(Integer profileId) {
        Profile profile = profileRepository.findById(profileId)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Profile not found"));

        profileRepository.delete(profile);
    }
}
