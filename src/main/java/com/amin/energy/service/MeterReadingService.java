package com.amin.energy.service;

import com.amin.energy.model.MeterReading;
import com.amin.energy.model.Month;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
public interface MeterReadingService {

    long getConsumption(int meterId, Month month);

    void delete(Long meterReadingId);

    MeterReading update(Long meterReadingId, MeterReading meterReading);

    MeterReading loadById(Long meterReadingId);

    MeterReading insert(MeterReading meterReading);
}
