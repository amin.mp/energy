package com.amin.energy.service;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
public interface MigrationService {

    void importMeterReadings(String meterReadingFilePath);

    void importFractions(String fractionFilePath);

}
