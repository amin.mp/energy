package com.amin.energy.service;

import com.amin.energy.model.Fraction;

import java.util.List;
import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
public interface FractionService {

    void delete(Integer fractionId);

    Fraction update(Integer fractionId, Fraction fraction);

    Fraction loadById(Integer fractionId);

    Fraction insert(Fraction fraction);

    List<Fraction> findAllByExample(Fraction example);

    Long countByExample(Fraction example);

    Optional<Fraction> findByExample(Fraction example);

}
