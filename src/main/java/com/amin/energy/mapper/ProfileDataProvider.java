package com.amin.energy.mapper;

import com.amin.energy.exception.ApiServiceException;
import com.amin.energy.exception.ErrorCode;
import com.amin.energy.model.Profile;
import com.amin.energy.model.dao.ProfileRepository;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Component
@Named("ProfileDataProvider")
public class ProfileDataProvider {

    @Autowired
    private ProfileRepository profileRepository;

    @Named("profile")
    public Profile getProfileByTitle(String profileTitle) {
        return profileRepository.findByTitle(profileTitle)
                .orElseThrow(() -> new ApiServiceException(ErrorCode.NOT_FOUND, "Profile not found: " + profileTitle));
    }
}
