package com.amin.energy.mapper;

import com.amin.energy.api.request.ProfileRequest;
import com.amin.energy.api.response.ProfileResponse;
import com.amin.energy.model.Profile;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 * Converts an entity to a response/request and vice versa. It uses MapStruct facilities in this respect.
 */
@Mapper(componentModel = "spring", uses = ProfileDataProvider.class,unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProfileMapper {

    /**
     * Converts an instance of Profile to a ProfileResponse
     * @param profile the source object
     * @return an instance of ProfileResponse
     */
    ProfileResponse profileToProfileResponse(Profile profile);

    /**
     * Converts an instance of ProfileRequest to a Profile
     * @param profileRequest the source object
     * @return an instance of Profile
     */
    Profile profileRequestToProfile(ProfileRequest profileRequest);

}
