package com.amin.energy.mapper;

import com.amin.energy.api.request.MeterReadingRequest;
import com.amin.energy.api.response.MeterReadingResponse;
import com.amin.energy.model.MeterReading;
import com.amin.energy.model.MeterReadingDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 * Converts an entity to a response/request and vice versa. It uses MapStruct facilities in this respect.
 */
@Mapper(componentModel = "spring", uses = ProfileDataProvider.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MeterReadingMapper {

    /**
     * Converts an instance of MeterReading to a MeterReadingResponse
     *
     * @param meterReading the source object
     * @return an instance of MeterReadingResponse
     */
    @Mapping(source = "meterReading.profile.title", target = "profile")
    @Mapping(expression = "java(com.amin.energy.model.Month.getMonth(meterReading.getMonth()).name())", target = "month")
    MeterReadingResponse meterReadingToMeterReadingResponse(MeterReading meterReading);

    /**
     * Converts an instance of MeterReadingRequest to a MeterReading
     *
     * @param meterReadingRequest the source object
     * @return an instance of MeterReading
     */
    @Mappings({ @Mapping(source = "meterReadingRequest.profile", target = "profile", qualifiedByName = { "ProfileDataProvider", "profile" }),
            @Mapping(expression = "java(com.amin.energy.model.Month.getValueByName(meterReadingRequest.getMonth()))", target = "month") })
    MeterReading meterReadingRequestToMeterReading(MeterReadingRequest meterReadingRequest);

    /**
     * Converts an instance of MeterReadingDto to a MeterReading
     *
     * @param meterReadingDto the source object
     * @return an instance of MeterReading
     */
    @Mappings({ @Mapping(source = "meterReadingDto.profile", target = "profile", qualifiedByName = { "ProfileDataProvider", "profile" }),
            @Mapping(expression = "java(com.amin.energy.model.Month.getValueByName(meterReadingDto.getMonth()))", target = "month") })
    MeterReading meterReadingDtoToMeterReading(MeterReadingDto meterReadingDto);
}
