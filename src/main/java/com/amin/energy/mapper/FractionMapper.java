package com.amin.energy.mapper;

import com.amin.energy.api.request.FractionRequest;
import com.amin.energy.api.response.FractionResponse;
import com.amin.energy.model.Fraction;
import com.amin.energy.model.FractionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 * Converts an entity to a response/request and vice versa. It uses MapStruct facilities in this respect.
 */
@Mapper(componentModel = "spring", uses = ProfileDataProvider.class,unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FractionMapper {

    /**
     * Converts an instance of Fraction to a FractionResponse
     * @param fraction the source object
     * @return an instance of FractionResponse
     */
    @Mappings({
            @Mapping(source = "fraction.ratio", target = "fraction"),
            @Mapping(source = "fraction.profile.title", target = "profile"),
            @Mapping(expression="java(com.amin.energy.model.Month.getMonth(fraction.getMonth()).name())", target = "month")
    })
    FractionResponse fractionToFractionResponse(Fraction fraction);

    /**
     * Converts an instance of FractionRequest to a Fraction
     * @param fractionRequest the source object
     * @return an instance of Fraction
     */
    @Mappings({
            @Mapping(source = "fractionRequest.fraction", target = "ratio"),
            @Mapping(source = "fractionRequest.profile", target = "profile", qualifiedByName = {"ProfileDataProvider", "profile"}),
            @Mapping(expression="java(com.amin.energy.model.Month.getValueByName(fractionRequest.getMonth()))", target = "month")
    })
    Fraction fractionRequestToFraction(FractionRequest fractionRequest);

    /**
     * Converts an instance of fractionDto to a Fraction
     * @param fractionDto the source object
     * @return an instance of Fraction
     */
    @Mappings({
            @Mapping(target = "profile", ignore = true),
            @Mapping(source = "fractionDto.fraction", target = "ratio"),
            @Mapping(expression="java(com.amin.energy.model.Month.getValueByName(fractionDto.getMonth()))", target = "month")
    })
    Fraction fractionDtoToFraction(FractionDto fractionDto);

}
