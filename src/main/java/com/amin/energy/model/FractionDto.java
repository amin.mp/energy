package com.amin.energy.model;

import java.math.BigDecimal;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
public class FractionDto {

    private String month;
    private String profile;
    private BigDecimal fraction;

    public FractionDto(String month, String profile, BigDecimal fraction) {
        this.month = month;
        this.profile = profile;
        this.fraction = fraction;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public BigDecimal getFraction() {
        return fraction;
    }

    public void setFraction(BigDecimal fraction) {
        this.fraction = fraction;
    }
}
