package com.amin.energy.model;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 */
public class MeterReadingDto {

    private Integer meterId;
    private String profile;
    private String month;
    private Long meterReading;

    public MeterReadingDto(Integer meterId, String profile, String month, Long meterReading) {
        this.meterId = meterId;
        this.profile = profile;
        this.month = month;
        this.meterReading = meterReading;
    }

    public Integer getMeterId() {
        return meterId;
    }

    public void setMeterId(Integer meterId) {
        this.meterId = meterId;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Long getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(Long meterReading) {
        this.meterReading = meterReading;
    }
}
