package com.amin.energy.model.dao;

import com.amin.energy.model.MeterReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Repository
public interface MeterReadingRepository extends JpaRepository<MeterReading, Long> {

    @Query("SELECT m.meterReading FROM MeterReading m where m.meterId = :meterId AND m.month = :month")
    Optional<Long> findMeterReadingByMeterIdAndMonth(@Param("meterId") int meterId,
                                                     @Param("month") int month);

}
