package com.amin.energy.model.dao;

import com.amin.energy.model.Fraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
@Repository
public interface FractionRepository extends JpaRepository<Fraction, Integer> {

    Optional<Fraction> findByMonthAndProfileId(Integer month, Integer profileId);
}
