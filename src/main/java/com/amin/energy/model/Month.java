package com.amin.energy.model;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 27 Jan 2019
 */
public enum Month {
    JAN(1),
    FEB(2),
    MAR(3),
    APR(4),
    MAY(5),
    JUN(6),
    JUL(7),
    AUG(8),
    SEP(9),
    OCT(10),
    NOV(11),
    DEC(12);

    private final int value;

    Month(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static int getValueByName(String monthName) {
        return Month.valueOf(monthName.toUpperCase()).getValue();
    }

    public boolean isFirstMonth() {
        return this.value == 1;
    }

    public static Month getMonth(int monthValue) {
        for (Month month : Month.values()) {
            if (month.value == monthValue) {
                return month;
            }
        }

        return null;
    }
}
