package com.amin.energy.exception;

import java.util.Optional;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 * An APIException is thrown if an attempt is made to make an invalid operation.
 */
public class ApiServiceException extends RuntimeException {

    private final Optional<ErrorCode> errorCode;

    /**
     * Constructs a new exception with the specified error code and detail message.
     *
     * @param errorCode the error code
     * @param message   the detail message
     */
    public ApiServiceException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = Optional.ofNullable(errorCode);
    }

    /**
     * Constructs a new exception with the specified error code, detail message and cause.
     *
     * @param errorCode the error code
     * @param message   the detail message
     * @param  cause the cause
     */
    public ApiServiceException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = Optional.ofNullable(errorCode);
    }

    public Optional<ErrorCode> getErrorCode() {
        return errorCode;
    }
}
