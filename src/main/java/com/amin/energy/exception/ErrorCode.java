package com.amin.energy.exception;

import org.springframework.http.HttpStatus;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Jan 2019
 * Enumeration of business error codes.
 */
public enum ErrorCode {
    NOT_FOUND(HttpStatus.NOT_FOUND),
    INVALID_PARAMETER(HttpStatus.BAD_REQUEST),
    INTERNAL_ERROR(HttpStatus.INTERNAL_SERVER_ERROR);

    private final HttpStatus value;

    ErrorCode(HttpStatus value) {
        this.value = value;
    }

    public HttpStatus getValue() {
        return value;
    }
}
