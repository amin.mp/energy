```
Energy Consumption
By Amin Malekpour.
---
Technology, Tool, and framework
These technologies, tools, and frameworks have been used in the project:
1. Java 8
2. Spring Boot (Web, Data-JPA, Test)
3. MapStruct
4. Maven
5. H2
6. SpringFox (swagger2, swagger-ui)

---

Project structure
I have used Maven as the build tool.
Source codes are located under the “energy/src/main/java” folder.
Besides, some codes will be generated at build time under the “energy/build/generated/source” folder.
Document
REST API documentation is provided via “Swagger-UI”. Besides, project document is available as
“Javadoc” under the “javadoc” folder.
Test
I have written some useful test classes under the “energy/src/test/java” folder. I used spring-boot-test
and JUnit to implement unit and integration tests. These tests were implemented by using facilities like
TestRestTemplate. If I had more time, I would implement some unit tests for business layer.
Bonus task
I have also done bonus task. A REST API has been implemented to import old-format CSV data into the
new application.
Run the application
Use the following command to build and run the application:
1. mvn clean install
2. mvn spring-boot:run

H2 console (http://localhost:8080/h2)
Swagger UI (http://localhost:8080/swagger-ui.html)
```